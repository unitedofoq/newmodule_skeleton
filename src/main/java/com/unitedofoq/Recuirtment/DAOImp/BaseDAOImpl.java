package com.unitedofoq.Recuirtment.DAOImp;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.unitedofoq.Recuirtment.DAOI.BaseDAOI;


public class BaseDAOImpl<T, ID extends Serializable>
        extends SimpleJpaRepository<T, ID> implements BaseDAOI<T, ID> {

    private final EntityManager entityManager;

   
        public BaseDAOImpl(JpaEntityInformation<T, ?>  entityInformation, EntityManager entityManager) {
          super(entityInformation, entityManager);
          this.entityManager = entityManager;
      }
     
    
  //here we will add method implemented from BASEDAO interface
        @Transactional
        public List<T> findByAttributeContainsText(String attributeName, String text) {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<T> cQuery = builder.createQuery(getDomainClass());
            Root<T> root = cQuery.from(getDomainClass());
            cQuery
              .select(root)
              .where(builder
                .like(root.<String>get(attributeName), "%" + text + "%"));
            TypedQuery<T> query = entityManager.createQuery(cQuery);
            return query.getResultList();
        }


		

}

   

