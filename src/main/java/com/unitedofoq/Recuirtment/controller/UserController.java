package com.unitedofoq.Recuirtment.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.unitedofoq.Recuirtment.Exception.*;

import com.unitedofoq.Recuirtment.DAOImp.UserDAO;
import com.unitedofoq.Recuirtment.entity.User;

import com.unitedofoq.Recuirtment.serviceImp.CrudUserServiceImp;

@RestController
//@RequestMapping("/User")
public class UserController {
	//Here we will create buisness logic
	//Retrieve all user records
	//Retrieve certain user(int id)
	
	@Autowired
	//these will hold buisness logic
	CrudUserServiceImp crudUserservice;
	@Autowired 
	UserDAO userDao;
	//@GetMapping("/users")
	//@PostMapping("/u")
	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> retrieveAllUsers() {
		List<User> user=crudUserservice.retrieveAllUsers();
		return user;
	}
	@GetMapping("/users/{id}")
	
	public Resource<User> retrieveUser(@RequestParam int id) {
		Resource<User> resource=crudUserservice.retrieveUser(id);
		return resource;
		}

//	 * @RequestMapping(value = "/users", method = RequestMethod.POST , produces =
//	 * MediaType.APPLICATION_JSON_VALUE, consumes =
//	 * MediaType.APPLICATION_JSON_VALUE)
//	 * 
//	 * public void createUser(@RequestBody User user) {
//	 * System.out.println("user:"+user.toString());
//	 * crudUserservice.createUser(user);
//	 * 
//	 * }
	// *///HATEOS Hyper media as the engine of application state
	 
	
	  @PostMapping("/users") public ResponseEntity<Object>
	  createUser2(@Valid @RequestBody User user) 
	  {
		  User savedUser =  crudUserservice.createUser2(user);
	  System.out.println("working user:"+user.toString());
	  URI location =
	  ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand
	  (savedUser.getClass()) .toUri();
	  
	  return ResponseEntity.created(location).build();
	  
	  }
	 
	 

//	@DeleteMapping("/users/{id}")
//	
//		public void deleteUser(@PathVariable int id) {
//			 userDao.deleteById(id);
//			
//		
//		
//	}
//	@DeleteMapping("/users/{id}")
//	public void deleteUser(@PathVariable int id) {
//	 userDao.deleteById(id);
//	
//	}

}
