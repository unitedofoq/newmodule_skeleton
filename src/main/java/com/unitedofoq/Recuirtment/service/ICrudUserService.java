package com.unitedofoq.Recuirtment.service;

import java.util.List;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.unitedofoq.Recuirtment.entity.User;

public interface ICrudUserService {

	List<User> 	retrieveAllUsers() throws Exception;
	public Resource<User> retrieveUser(@PathVariable int id) throws Exception;
	public void  createUser(@RequestBody User user) throws Exception;
	public void deleteUser(@PathVariable int id) throws Exception;
}
