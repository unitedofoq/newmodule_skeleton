package com.unitedofoq.Recuirtment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.unitedofoq.Recuirtment.DAOImp.BaseDAOImpl;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories(repositoryBaseClass = BaseDAOImpl.class)
@ComponentScan
public class RecruitmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecruitmentApplication.class, args);
	}

}
