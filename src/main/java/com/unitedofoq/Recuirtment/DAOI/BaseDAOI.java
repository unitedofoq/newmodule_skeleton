/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitedofoq.Recuirtment.DAOI;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;



@NoRepositoryBean
public interface BaseDAOI<T, ID extends Serializable> extends JpaRepository<T, ID> {
//Here is an interface that will include different crud methods not exists on JPA reporistry
	 
	  public List<T> findByAttributeContainsText(String attributeName, String text);
	  
	  //Here IF we Need To create DifFerent Genric Methods
	  
	  

	  
  
}
