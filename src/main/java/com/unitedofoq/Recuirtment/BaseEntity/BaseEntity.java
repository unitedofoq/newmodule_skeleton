package com.unitedofoq.Recuirtment.BaseEntity;

import javax.persistence.Transient;

//Entity that will contain shared properties between entities
//This is an Example and we will Determine according to Buisness 
public class BaseEntity {
	 static private long stInstID;
	 @Transient
	    private Long instID;

	    public Long getInstID() {
	        return instID;
	    }

	    public void setInstID(Long instID) {
	        this.instID = instID;
	    }

	    public void generateInstID() {
	        if (instID == null) {
	            stInstID++;
	            instID = stInstID;
	        }
	    }
	    // </editor-fold>

	    public void BaseEntity() {
	        // Instance Management
	        if (instID == null) {
	            stInstID++;
	            instID = stInstID;
	        }
	    }
}
