package com.unitedofoq.Recuirtment.serviceImp;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.unitedofoq.Recuirtment.DAOImp.UserDAO;
import com.unitedofoq.Recuirtment.Exception.UserNotFoundException;
import com.unitedofoq.Recuirtment.entity.User;
import com.unitedofoq.Recuirtment.service.ICrudUserService;

@Service
@Transactional
public class CrudUserServiceImp implements ICrudUserService {
@Autowired
UserDAO userDao;
private static List<User> users = new ArrayList<>();
	public List<User> retrieveAllUsers() {
		return userDao.findAll();
	}
	
	public Resource<User> retrieveUser(@PathVariable int id) {
		Optional<User> user=userDao.findById(id);
		if (!user.isPresent())
			throw new UserNotFoundException("id-" + id);

		// "all-users", SERVER_PATH + "/users"
		// retrieveAllUsers
		Resource<User> resource = new Resource<User>(user.get());
		ControllerLinkBuilder linkTo=linkTo(methodOn(this.getClass()).retrieveAllUsers());
		resource.add(linkTo.withRel("all-users"));
		return resource;
	}
	
		public  User createUser2(@RequestBody User user) {
			System.out.println("user 2:"+user.toString());
		
		 User savedUser =  userDao.save(user);
		  System.out.println("working savedUser:"+savedUser.toString());
		 
		  
		  return savedUser;
		  
		
//		URI location = ServletUriComponentsBuilder
//				.fromCurrentRequest()
//				.path("/")
//				.buildAndExpand(savedUser.getId()).toUri();
//		return ResponseEntity.created(location).build();
			
		}
	//HATEOS Hyper media as the engine of application state
		public List<User> findAll() {
			return users;
		}


	
	public void deleteUser(@RequestParam int id) {
		 userDao.deleteById(id);
		
		
	}

	@Override
	public void createUser(User user) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
