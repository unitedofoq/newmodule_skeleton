package com.unitedOfoq.RecuirtmentService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class RecuirtmentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecuirtmentServiceApplication.class, args);
	}

}
